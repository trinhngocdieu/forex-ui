# FOREX-UI APP

## Introduction
Forex App is an application let you search and build the dream pokemon team. <br />
You also can see a demo video of this app at the following link:

[Demo Video](https://drive.google.com/file/d/1ZLeuj3BNi6Ta_W61NnpIbF_Zi_lSTj-d/view?usp=sharing)

## Getting started
You can use `npm` or `yarn` to execute the following commands. <br />

We recommend `yarn` for better performance and security. <br />
You might need to install `yarn` using this command: 
`npm install --global yarn`

Firstly, you need to install the dependencies:
### `yarn install`

After that, in the project directory, you can run:
### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn lint`
Using eslint to check for any errors in yours code to ensure the code quality and coding style of the project.

### `yarn lint:fix`
Fix the above errors automatically, this command not guarantee fixing all the errors, some errors you need to fix them manually.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.
