export interface Rate {
  from: string;
  to: string;
  bid: number;
  ask: number;
  price: number;
  time_stamp: string;
}

export enum DIRECTION {
  UP = 'UP',
  DOWN = 'DOWN',
  SAME = 'SAME',
}

