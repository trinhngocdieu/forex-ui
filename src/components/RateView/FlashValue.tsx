import { PRECISION } from 'config';
import { DIRECTION } from 'types';
import Arrow from './Arrow';
import styles from './FlashValue.module.scss';

type IFlashValueProps = {
  currentValue: number,
  prevValue: number,
};

const FlashValue = ({ prevValue, currentValue }: IFlashValueProps) => {
  const getDirection = () => {
    if (prevValue < currentValue) {
      return DIRECTION.UP;
    } else if (prevValue > currentValue) {
      return DIRECTION.DOWN;
    } else {
      return DIRECTION.SAME;
    }
  };

  const direction = getDirection();

  return (
    <div className={styles.main}>
      <span className={styles[direction]}>
        <Arrow direct={direction} />
        {Number(currentValue).toFixed(PRECISION)}
      </span>
    </div>
  )
};

export default FlashValue;
