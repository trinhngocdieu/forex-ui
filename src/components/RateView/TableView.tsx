import { useAppSelector } from 'hooks'
import { selectRates } from 'store/rate/rateSlice'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import FlashValue from './FlashValue';

const TableView = () => {
  const { rates, currentRate } = useAppSelector(selectRates);
  if (!currentRate) {
    return null;
  }
  const prevRate = rates.length > 1 ?
    rates[rates.length - 2] :
    currentRate;

  const currentTime = new Date(currentRate?.time_stamp)
    .toLocaleTimeString();

  return (
    <TableContainer
      component={Paper}
    >
      <Table aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Time</TableCell>
            <TableCell>Bid</TableCell>
            <TableCell>Price</TableCell>
            <TableCell>Ask</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow>
            <TableCell>
              {currentTime}
            </TableCell>
            <TableCell>
              <FlashValue
                currentValue={currentRate.bid}
                prevValue={prevRate.bid}
              />
            </TableCell>
            <TableCell>
              <FlashValue
                currentValue={currentRate.price}
                prevValue={prevRate.price}
              />
            </TableCell>
            <TableCell>  
              <FlashValue
                currentValue={currentRate.ask}
                prevValue={prevRate.ask}
              />
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  )
}

export default TableView;
