import FlashValue from 'components/RateView/FlashValue';
import { screen } from '@testing-library/dom';
import { renderWithContext } from 'test-utils';
import { cleanup } from '@testing-library/react';

describe('FlashValue', () => {
  afterEach(cleanup);

  test('should render with up arrow when currentValue is higher than prevValue', () => {
    renderWithContext(
      <FlashValue
        currentValue={1}
        prevValue={0}
      />,
    );

    const upArrow = screen.getByText(/↑/i);
    expect(upArrow).toBeInTheDocument();
  });

  test('should render with down arrow when currentValue is lower than prevValue', () => {
    renderWithContext(
      <FlashValue
        currentValue={0}
        prevValue={1}
      />,
    );

    const downArrow = screen.getByText(/↓/i);
    expect(downArrow).toBeInTheDocument();
  });
});
