import { cleanup } from '@testing-library/react';
import Arrow from 'components/RateView/Arrow';
import { renderWithContext } from 'test-utils';
import { DIRECTION } from 'types';

describe('Arrow', () => {
  afterEach(cleanup);

  test('should render up arrow with direction UP', () => {
    const { getByText } = renderWithContext(
      <Arrow direct={DIRECTION.UP} />,
    );

    const upArrow = getByText(/↑/i);
    expect(upArrow).toBeInTheDocument();
  });

  test('should render down arrow with direction DOWN', () => {
    const { getByText } = renderWithContext(
      <Arrow direct={DIRECTION.DOWN} />,
    );

    const downArrow = getByText(/↓/i);
    expect(downArrow).toBeInTheDocument();
  });

  test('should render no arrow with direction SAME', () => {
    const { queryByText } = renderWithContext(
      <Arrow direct={DIRECTION.SAME} />,
    );

    const upArrow = queryByText(/↑/i);
    const downArrow = queryByText(/↓/i);
    
    expect(upArrow).not.toBeInTheDocument();
    expect(downArrow).not.toBeInTheDocument();
  });
});
