import TableView from 'components/RateView/TableView';
import { RootState } from 'store';
import { renderWithContext } from 'test-utils';

describe('TableView', () => {
  it('should render correctly', () => {
    const USDJPYRate = {
      from: 'USD',
      to: 'JPY',
      bid: 0.4,
      ask: 0.5,
      price: 0.45,
    };

    const currentState = {
      baseCurrency: 'USD',
      quoteCurrency: 'EUR',
      rates: [USDJPYRate],
      currentRate: USDJPYRate,
    };

    const { container } = renderWithContext(
      <TableView />,
      { rate: currentState } as RootState,
    );

    const tableView = container.querySelector('table');
    expect(tableView).toBeInTheDocument();
  });
});
