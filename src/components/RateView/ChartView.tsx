import * as agCharts from 'ag-charts-community';
import { AgChartsReact } from 'ag-charts-react';
import { Rate } from 'types';

type IChartView = {
  rates: Rate[];
};

const ChartView = ({ rates }: IChartView) => {
  const ratesData = rates.map(rate => ({
    ...rate,
    time_stamp: new Date(rate.time_stamp)
  }));

  const options = {
    autoSize: true,
    series: [
      {
        data: ratesData,
        xKey: 'time_stamp',
        yKey: 'bid',
        yName: 'Bid',
        stroke: 'green',
        marker: {
          fill: 'green',
          stroke: 'darkgreen',
        },
      },
      {
        data: ratesData,
        xKey: 'time_stamp',
        yKey: 'price',
        yName: 'Price',
        stroke: 'grey',
        marker: {
          fill: 'grey',
          stroke: 'darkgrey',
        },
      },
      {
        data: ratesData,
        xKey: 'time_stamp',
        yKey: 'ask',
        yName: 'Ask',
        stroke: 'red',
        marker: {
          fill: 'red',
          stroke: 'firebrick',
        },
      },
      
    ],
    axes: [
      {
        type: 'time',
        position: 'bottom',
        tick: {
          count: agCharts.time.second.every(1),
        },
        label: {
          format: '%H:%M:%S',
        },
        
      },
      {
        type: 'number',
        position: 'left',
        label: {
          format: '#{.2f}',
        },
      },
    ],
    legend: {
      enabled: true,
    },
  }

  return (
    <div data-testid="chart-view">
      <AgChartsReact
        options={options}
      />
    </div>
  )
};

export default ChartView;
