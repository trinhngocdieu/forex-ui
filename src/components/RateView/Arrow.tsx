import { DIRECTION } from 'types';

type IArrowProps = {
  direct: DIRECTION;
};

const Arrow = ({ direct }: IArrowProps) => {
  switch (direct) {
    case DIRECTION.UP:
      return <span>{'↑ '}</span>;
    case DIRECTION.DOWN:
      return <span>{'↓ '}</span>;
    case DIRECTION.SAME:
      return <></>; 
    default:
      return null;
  }
};

export default Arrow;
