import { useState } from 'react'
import { codes as currencyCodes } from 'currency-codes';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select, { SelectChangeEvent } from '@mui/material/Select';

type ICurrencySelector = {
  label: string,
  onSelectCurrency: (currencyCode: string) => void,
}

export default function CurrencySelector({ label, onSelectCurrency }: ICurrencySelector) {
  const [currency, setCurrency] = useState('');
  const currencyList = currencyCodes();

  const handleChange = (event: SelectChangeEvent) => {
    setCurrency(event.target.value);
    onSelectCurrency(event.target.value);
  };

  return (
    <Box
      sx={{ minWidth: 120 }}
      data-testid={`currency-selector-${label}`}
    >
      <FormControl fullWidth>
        <InputLabel id={label}>
          {label}
        </InputLabel>
        <Select
          labelId={label}
          value={currency}
          label={label}
          onChange={handleChange}
        >
          {currencyList.map(code => (
            <MenuItem
              key={code}
              value={code}
            >
              {code}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  )
}
