import { renderWithContext } from 'test-utils';
import CurrencySelector from '../CurrencySelector';

describe('Currency Selector', () => {
  test('should render Currency Selector correctly', () => {
    const { getByTestId } = renderWithContext(
      <CurrencySelector
        label='test-label'
        onSelectCurrency={() => {
          // noop
        }}
      />,
    );

    const currencySelectorEl = getByTestId('currency-selector-test-label');
    expect(currencySelectorEl).toBeInTheDocument();
  });
});
