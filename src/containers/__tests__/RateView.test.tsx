import { act, cleanup } from '@testing-library/react';
import RateView from 'containers/RateView';
import { RootState } from 'store';
import { renderWithContext } from 'test-utils';

jest.mock('components/RateView/ChartView', () => {
  return () => <div data-testid="chart-view" />;
});

const updateWrapper = async (wrapper: HTMLElement, time = 10) => {
  await act(async () => {
    await new Promise(res => setTimeout(res, time));
  });
};

describe('Rate View', () => {
  afterEach(cleanup);

  test('should render select a pair instruction when from currency or to currency not selected', () => {
    const currentState = {
      baseCurrency: '',
      quoteCurrency: '',
      rates: [],
      currentRate: undefined,
    };
    const { getByTestId } = renderWithContext(
      <RateView />,
      { rate: currentState } as RootState,
    );
    
    const notSelected = getByTestId('not-selected');

    expect(notSelected).toBeInTheDocument();
    expect(notSelected).toHaveTextContent('Please select a pair from the above dropdowns');
  });

  test('should render please selecting different currencies when from currency and to currency are the same', () => {
    const currentState = {
      baseCurrency: 'USD',
      quoteCurrency: 'USD',
      rates: [],
      currentRate: undefined,
    };
    const { getByTestId } = renderWithContext(
      <RateView />,
      { rate: currentState } as RootState,
    );

    const sameCurrency = getByTestId('same-currency');
    expect(sameCurrency).toBeInTheDocument();
    expect(sameCurrency).toHaveTextContent('Please select different currencies');
  });

  test('should show loading when loading the streaming data', () => {
    const currentState = {
      baseCurrency: 'USD',
      quoteCurrency: 'EUR',
      rates: [],
      currentRate: undefined,
    };
    const { getByTestId } = renderWithContext(
      <RateView />,
      { rate: currentState } as RootState,
    );

    const loading = getByTestId('loading');
    expect(loading).toBeInTheDocument();
  });

  test('should render rate view data when rate is streaming', async () => {
    const currentState = {
      baseCurrency: 'USD',
      quoteCurrency: 'EUR',
      rates: [],
      currentRate: undefined,
    };

    global.fetch = jest.fn(() =>
      Promise.resolve({
        json: () => Promise.resolve({
          from: 'USD',
          to: 'EUR',
          bid: 0.2,
          price: 0.7,
          ask: 0.9,
          time_stamp: '2020-01-01T00:00:00.000Z',
        }),
      }),
    ) as jest.Mock;

    const { getByTestId, container } = renderWithContext(
      <RateView />,
      { rate: currentState } as RootState,
    );

    await updateWrapper(container);

    const tableViewEl = await getByTestId('rate-view');
    expect(tableViewEl).toBeInTheDocument();
  });
});
