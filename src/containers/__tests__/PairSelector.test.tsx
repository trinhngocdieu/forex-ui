import PairSelector from 'containers/PairSelector';
import { renderWithContext } from 'test-utils';

describe('Pair Selector', () => {
  it('should render PairSelector correctly', () => {
    const { getByTestId } = renderWithContext(
      <PairSelector />,
    );
    const fromSelector = getByTestId('from');
    const toSelector = getByTestId('to');

    expect(fromSelector).toBeInTheDocument();
    expect(toSelector).toBeInTheDocument();
  });
});
