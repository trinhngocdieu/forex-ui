import CurrencySelector from 'components/CurrencySelector/CurrencySelector';
import { useAppDispatch } from 'hooks';
import { setBaseCurrency, setQuoteCurrency } from 'store/rate/rateSlice';
import styles from "./PairSelector.module.scss";

export default function PairSelector () {
  const dispatch = useAppDispatch();
  
  const onSetBase = (base: string) => {
    dispatch(setBaseCurrency(base));
  };

  const onSetQuote = (quote: string) => {
    dispatch(setQuoteCurrency(quote));
  };

  return (
    <div className={styles.main}>
      <div data-testid="from">
        <CurrencySelector
          label="From"
          onSelectCurrency={onSetBase}
        />
      </div>
     
      <span className={styles.separator}>/</span>
      
      <div data-testid="to">
        <CurrencySelector
          label="To"
          onSelectCurrency={onSetQuote}
        />
      </div>
    </div>
  )
}
