import { useAppDispatch, useAppSelector } from 'hooks'
import { selectRates } from 'store/rate/rateSlice'
import { useGetRateStreaming } from 'hooks/useGetRateStreaming';
import { CircularProgress } from '@mui/material';
import ChartView from 'components/RateView/ChartView';
import TableView from 'components/RateView/TableView';
import styles from './RateView.module.scss';

export default function RateView() {
  const dispatch = useAppDispatch();

  const { rates, baseCurrency, quoteCurrency } = useAppSelector(selectRates);
  const { isLoading } = useGetRateStreaming(baseCurrency, quoteCurrency, dispatch);

  const notSelected = !baseCurrency || !quoteCurrency;
  const sameCurrency = baseCurrency === quoteCurrency;

  return (
    <div className={styles.main}>
      {notSelected ? 
        <section
          data-testid="not-selected"
          className={styles.select}
        >
          Please select a pair from the above dropdowns
        </section>:
        <section>
          {sameCurrency ?
            <div
              data-testid="same-currency"
              className={styles.select}
            >
              Please select different currencies
            </div> :
            <>
              {isLoading ?
                <div
                  data-testid="loading"
                  className={styles.loading}
                >
                  <CircularProgress />
                </div>:
                <div data-testid="rate-view">
                  <TableView />
                  <ChartView rates={rates} />
                </div>
              }
            </>
          }
        </section>
      }
    </div>
  )
}
