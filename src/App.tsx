import PairSelector from 'containers/PairSelector';
import RateView from 'containers/RateView';
import HeaderImage from 'assets/header.png';
import styles from './App.module.scss';

function App() {
  return (
    <div className={styles.App}>
      <header>
        <img
          src={HeaderImage}
          width={300}
          height={200}
          data-testid="header"
        />     
      </header>
      <main>
        <PairSelector />
        <RateView />
      </main>
    </div>
  );
}

export default App;
