import React from 'react';
import { Provider } from 'react-redux';
import { render } from "@testing-library/react";
import { RootState } from 'store';
import { configureStore } from '@reduxjs/toolkit';
import rateReducer from 'store/rate/rateSlice';

const reducer = {
  rate: rateReducer
};

const getStoreWithState = (state?: RootState) => {
  return configureStore({
    reducer,
    preloadedState: state as RootState
  });
};

export function renderWithContext(
  element: React.ReactElement,
  state?: RootState,
) {
  const store = getStoreWithState(state);
  const utils = render(
    <Provider store={store}>
      {element}
    </Provider>
  );
  return { store, ...utils };
}
