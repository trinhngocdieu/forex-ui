import { Dispatch, useEffect, useState } from 'react';
import { RateState, setRate } from 'store/rate/rateSlice';
import { ThunkDispatch, AnyAction } from '@reduxjs/toolkit';
import { API_HOST, TOKEN } from "config";

export const useGetRateStreaming = (
  baseCurrency: string,
  quoteCurrency: string,
  dispatch: ThunkDispatch<{
    rate: RateState;
  }, undefined, AnyAction> & Dispatch<AnyAction>
) => {
  const [isLoading, setIsLoading] = useState(true);
  let reader: ReadableStreamDefaultReader<string>;

  const fetchData = async () => {
    setIsLoading(true);
    const pair = `${baseCurrency}${quoteCurrency}`;
  
    const response = await fetch(`${API_HOST}${pair}`, {
      method: "GET",
      headers: {
        token: TOKEN
      }
    });

    if (response !== null) {
      setIsLoading(false);

      if (response.body) {
        reader = response.body
          .pipeThrough(
            new TextDecoderStream()
          ).getReader();
  
        while (true) {
          const { value, done } = await reader.read();
          if (done || !value) break;
          const rateData = JSON.parse(value).pop();
          dispatch(setRate(rateData));
        }
      }
    }
  };

  useEffect(() => {
    if (baseCurrency && quoteCurrency) {
      fetchData();
    }
    return () => {
      reader && reader.cancel();
    };
  }, [baseCurrency, quoteCurrency]);

  return { isLoading };
}


  
