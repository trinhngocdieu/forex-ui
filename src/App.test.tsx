import App from './App';
import { renderWithContext } from 'test-utils';

describe('App', () => {
  test('should render app correctly', () => {
    const { getByTestId } = renderWithContext(<App />);
    expect(getByTestId('header')).toBeInTheDocument();
  });
});
