import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import rateReducer from './rate/rateSlice';

export const store = configureStore({
  reducer: {
    rate: rateReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
