import rateReducer, { initialState, setBaseCurrency, setQuoteCurrency, setRate } from 'store/rate/rateSlice';

describe('rate reducer', () => {
  it('should hanlde initial state', () => {
    expect(
      rateReducer(undefined, { type: 'unknown' })
    ).toEqual(initialState);
  });

  it('should hanlde set base currency', () => {
    const actual = rateReducer(initialState, setBaseCurrency('USD'));
    expect(actual.baseCurrency).toEqual('USD');
  });

  it('should hanlde set quote currency', () => {
    const actual = rateReducer(initialState, setQuoteCurrency('JPY'));
    expect(actual.quoteCurrency).toEqual('JPY');
  });

  it('should hanlde set rates', () => {
    const SGDAUDRate = {
      from: 'SGD',
      to: 'AUD',
      time_stamp: '2020-01-01T00:00:00.000Z',
      bid: 0.3,
      price: 0.5,
      ask: 0.6
    };

    const currentState = {
      rates: [SGDAUDRate],
      currentRate: SGDAUDRate,
      baseCurrency: 'SGD',
      quoteCurrency: 'AUD'
    };

    const USDJPYRate = {
      time_stamp: '2020-01-01T00:00:00.000Z',
      from: 'USD',
      to: 'JPY',
      bid: 0.4,
      price: 0.6,
      ask: 0.7
    };
    
    const actual = rateReducer(currentState, setRate(USDJPYRate));

    expect(actual.rates).toEqual([...currentState.rates, USDJPYRate]);
    expect(actual.currentRate).toEqual(USDJPYRate);
  });
});
