import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { MAX_RATE_SERIES_LENGTH } from "config";
import { RootState } from "store";
import { Rate } from "types";

export interface RateState {
  rates: Rate[];
  currentRate?: Rate;
  baseCurrency: string;
  quoteCurrency: string;
}

export const initialState: RateState = {
  rates: [],
  currentRate: undefined,
  baseCurrency: '',
  quoteCurrency: '',
};

const rateSlice = createSlice({
  name: "rate",
  initialState,
  reducers: {
    setRate: (state, action: PayloadAction<Rate>) => {
      const rate = action.payload;
      state.currentRate = rate;
      state.rates.push(rate);
      if (state.rates.length > MAX_RATE_SERIES_LENGTH) {
        state.rates.shift();
      }
    },
    setBaseCurrency: (state, action: PayloadAction<string>) => {
      state.baseCurrency = action.payload;
      state.rates = [];
    },
    setQuoteCurrency: (state, action: PayloadAction<string>) => {
      state.quoteCurrency = action.payload;
      state.rates = [];
    }
  },
});

export const {
  setRate,
  setBaseCurrency,
  setQuoteCurrency,
} = rateSlice.actions;

export const selectRates = (state: RootState) => state.rate;

export default rateSlice.reducer;
